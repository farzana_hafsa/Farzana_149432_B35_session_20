<?php

namespace App;

class Person
{

    public $name="Farzana";
    public $gender="Female";
    public $blood_group= "B+";


    public function showPersonInfo(){
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }

}